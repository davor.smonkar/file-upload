import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';

@Injectable()
export class ApiService {
    private apiUrl = 'http://localhost:3001';
    constructor(private http: HttpClient){}
    
    upload(formData: FormData): Observable<HttpEvent<string[]>> {
        return this.http.post<string[]>(`${this.apiUrl}/upload`, formData, {
            reportProgress: true,
            observe: 'events'
        });
    }
}