import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FileUploadService } from 'src/services/file-upload.service';

@Component({
  selector: 'app-upload-files',
  templateUrl: './upload-files.component.html',
  styleUrls: ['./upload-files.component.scss'],
})



export class FileUploadComponent implements OnInit {
  @ViewChild('fileInput') input!: ElementRef<HTMLInputElement>;

  public Status = {
    Init: 1,
    Selected:  2,
    Uploading: 3,
    Error: 4,
  }

  selectedFiles?: FileList;
  progressInfos: any[] = [];
  imageInfos?: Observable<any>;
  uploading: boolean = false;
  allfilesFailedError: boolean = false;
  numberOfSelectedFiles: any = 0;
  status: number = this.Status.Init;

  constructor(private uploadService: FileUploadService, private changeDetector: ChangeDetectorRef) { }

  ngOnInit(): void {}

  onFileChanged(event: any) {
    this.selectFiles(event);
  }

  selectFiles(event: any, droppable: boolean = false): void {
    this.progressInfos = [];
    this.selectedFiles = droppable ? event : event.target.files;
    this.numberOfSelectedFiles = this.selectedFiles?.length;
    this.status = this.Status.Selected;
  }

  uploadFiles(file: any = null): void {
    if (file) {
      this.upload(this.progressInfos.indexOf(file), file.fileObject);
    }
    else {
      if (this.selectedFiles) {
        for (let i = 0; i < this.selectedFiles.length; i++) {
          this.upload(i, this.selectedFiles[i]);
        }
      }
    }
  }

  upload(idx: number, file: File): void {
    this.status = this.Status.Uploading;
    this.progressInfos[idx] = { value: 0, fileName: file.name, size: this.formatBytes(file.size), errorMessage: '', fileObject: file, idx: idx };

    if (file) {
      this.uploadService.upload(file).subscribe(
        (event: any) => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progressInfos[idx].value = Math.round(100 * event.loaded / event.total);
          } 
          else if (event instanceof HttpResponse) {
            if (this.progressInfos.every(info => info.errorMessage != '')) {
              this.status = this.Status.Error;
            }
            else {
              this.status = this.Status.Uploading;
            }
          }
        },
        (error: any) => {
          const msg = 'Error uploading file';
          this.progressInfos[idx].errorMessage = msg;
          if (this.progressInfos.every(info => info.errorMessage != '')) {
            this.status = this.Status.Error;
          }
          else {
            this.status = this.Status.Uploading;
          }
        });
    }
  }

  removeItem(item: any) {
    this.progressInfos = this.progressInfos.filter(function (info) {
      return item.fileName != info.fileName;
    });
    if (this.progressInfos.length == 0) {
      this.clearSelection();
    }
  }

  clearSelection() {
    this.progressInfos = [];
    this.input.nativeElement.value = "";
    this.numberOfSelectedFiles = 0;
    this.status = this.Status.Init;
  }

  formatBytes(bytes: number) {
    var marker = 1024;
    var decimal = 3;
    var kiloBytes = marker;
    var megaBytes = marker * marker;
    var gigaBytes = marker * marker * marker;

    // return bytes if less than a KB
    if (bytes < kiloBytes) return bytes + " Bytes";
    // return KB if less than a MB
    else if (bytes < megaBytes) return (bytes / kiloBytes).toFixed(decimal) + " KB";
    // return MB if less than a GB
    else if (bytes < gigaBytes) return (bytes / megaBytes).toFixed(decimal) + " MB";
    // return GB if less than a TB
    else return (bytes / gigaBytes).toFixed(decimal) + " GB";
  }
}