import { Directive, HostBinding, HostListener, Output, EventEmitter } from "@angular/core";

@Directive({
  selector: "[appDrag]"
})
export class DragDirective {
  @Output() fileDropped: EventEmitter<any> = new EventEmitter();

  @HostBinding("style.background") private background = "#282828";

  constructor() { }

  @HostListener("dragover", ["$event"]) public onDragOver(evt: DragEvent) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = "rgb(48 48 48)";
  }

  @HostListener("dragleave", ["$event"]) public onDragLeave(evt: DragEvent) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#282828';
  }

  @HostListener('drop', ['$event']) public onDrop(evt: DragEvent) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#282828';
    if (evt && evt.dataTransfer && evt.dataTransfer.files) {
      this.fileDropped.emit(evt.dataTransfer.files);
    }
  }
}