import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ApiService } from 'src/services/api.service';
import { FileUploadService } from 'src/services/file-upload.service';

import { AppComponent } from './app.component';
import { FileUploadComponent } from './components/upload-files.component';
import { DragDirective } from './directives/dragDrop.directive';

@NgModule({
  declarations: [
    AppComponent,
    FileUploadComponent,
    DragDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [ApiService, FileUploadService],
  bootstrap: [AppComponent]
})
export class AppModule { }
